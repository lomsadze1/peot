package com.example.finalapplication

import android.media.AudioManager
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.io.IOException

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigation)

        val controller = findNavController(R.id.nav_graph)


        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.home,
                R.id.vazha,
                R.id.galaktion
            )
        )

        setupActionBarWithNavController(controller, appBarConfiguration)
        bottomNavigationView.setupWithNavController(controller)

    }

    var url: String = "https://dl17.320ytmp3.com/file/youtubeTUDQHPIY-Uw128.mp3?fn=%E2%9C%94%20%E1%83%92%E1%83%90%E1%83%9A%E1%83%90%E1%83%99%E1%83%A2%E1%83%98%E1%83%9D%E1%83%9C%20%E1%83%A2%E1%83%90%E1%83%91%E1%83%98%E1%83%AB%E1%83%94%20-%20%2C%2C%E1%83%9B%E1%83%97%E1%83%90%E1%83%AC%E1%83%9B%E1%83%98%E1%83%9C%E1%83%93%E1%83%98%E1%83%A1%20%E1%83%9B%E1%83%97%E1%83%95%E1%83%90%E1%83%A0%E1%83%94%E2%80%9C%20(1915)%20_%20Galaktion%20Tabidze%20-%20Mtatsmindis%20Mtvare.mp3"
    var mediaPlayer = MediaPlayer()

    fun playsong(view: View) {
        mediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
        if (!mediaPlayer!!.isPlaying) {

            Toast.makeText(this, "Playing", Toast.LENGTH_SHORT).show()
            try {
                mediaPlayer!!.setDataSource(url)
                mediaPlayer.prepare()
                mediaPlayer.start()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else {
            Toast.makeText(this, "stopped", Toast.LENGTH_SHORT).show()
            try {
                mediaPlayer.pause()
                mediaPlayer.stop()
                mediaPlayer.reset()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

}