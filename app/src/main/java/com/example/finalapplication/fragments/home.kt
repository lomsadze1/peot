package com.example.finalapplication.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.finalapplication.R

class home : Fragment(R.layout.home_fragment) {

    lateinit var button: Button
    lateinit var button1: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button = view.findViewById(R.id.button)

        button1 = view.findViewById(R.id.button1)

        val navController = Navigation.findNavController(view)


        button.setOnClickListener {

            val action = homeDirections.actionHomeToVazha()
            navController.navigate(action)

        }

        button1.setOnClickListener {

            val action1 = homeDirections.actionHomeToGalaktion()
            navController.navigate(action1)

        }


    }


}